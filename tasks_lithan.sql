-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: tasks_lithan
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `certificate`
--

DROP TABLE IF EXISTS `certificate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `certificate` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `img` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificate`
--

LOCK TABLES `certificate` WRITE;
/*!40000 ALTER TABLE `certificate` DISABLE KEYS */;
INSERT INTO `certificate` VALUES (1,'artificial intelgence','0001102.png','2022-03-24 13:51:41',NULL),(2,'sofware tester','000389.jpeg','2022-03-24 13:51:41',NULL),(3,'data analys','0000012.png','2022-03-24 13:51:41',NULL),(4,'database enginer','000203.jpeg','2022-03-24 13:51:41',NULL),(5,'fullstack developer','339i100.png','2022-03-24 13:51:41',NULL),(6,'frontend','00029487.png','2022-03-24 13:51:41',NULL);
/*!40000 ALTER TABLE `certificate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comentar`
--

DROP TABLE IF EXISTS `comentar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comentar` (
  `id` int NOT NULL AUTO_INCREMENT,
  `coment` text NOT NULL,
  `created_by` int NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentar`
--

LOCK TABLES `comentar` WRITE;
/*!40000 ALTER TABLE `comentar` DISABLE KEYS */;
INSERT INTO `comentar` VALUES (1,'hallo sir..im interested',2,'2022-03-23 02:22:20',NULL,NULL),(2,'you can send ur cv',1,'2022-03-23 02:22:20',NULL,NULL),(3,'hallo sir..i\'ve already sent my cv to your email please check it sirr thanks a lot',3,'2022-03-23 02:22:20',NULL,NULL),(4,'okay..i\'ll check later...',1,'2022-03-23 02:22:20',NULL,NULL),(5,'how much the salary per mounth sirr??',4,'2022-03-23 02:22:20',NULL,NULL),(6,'Hallo mr alliano im interesteerd and i\'ve already send my cv to ur email..please check it sir..',5,'2022-03-23 04:28:41',NULL,NULL);
/*!40000 ALTER TABLE `comentar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eduVideos`
--

DROP TABLE IF EXISTS `eduVideos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eduVideos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `videosName` varchar(255) DEFAULT NULL,
  `idVideo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_fideos` (`idVideo`),
  CONSTRAINT `fk_fideos` FOREIGN KEY (`idVideo`) REFERENCES `education` (`listVideos`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eduVideos`
--

LOCK TABLES `eduVideos` WRITE;
/*!40000 ALTER TABLE `eduVideos` DISABLE KEYS */;
INSERT INTO `eduVideos` VALUES (1,'What is Server Administrator','A000P1'),(2,'What is server','A000P1'),(3,'Learn command line server','A000P1'),(4,'Set Ip server','A000P1'),(5,'Manage resource memory ','A000P1'),(6,'How to backup data server','A000P1');
/*!40000 ALTER TABLE `eduVideos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `education` (
  `id` int NOT NULL AUTO_INCREMENT,
  `eduName` varchar(255) NOT NULL,
  `applay_by` int DEFAULT NULL,
  `listVideos` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `listVideos` (`listVideos`),
  UNIQUE KEY `listVideos_2` (`listVideos`),
  KEY `fk_edu_user` (`applay_by`),
  CONSTRAINT `fk_edu_user` FOREIGN KEY (`applay_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `education`
--

LOCK TABLES `education` WRITE;
/*!40000 ALTER TABLE `education` DISABLE KEYS */;
INSERT INTO `education` VALUES (1,'Learn Server Administrator',1,'A000P1','2022-04-06 02:54:53',NULL,NULL),(2,'Mysql Server & Client',1,'A000P2','2022-04-06 02:54:53',NULL,NULL),(3,'Learn Basic Typescript',3,'A000P3','2022-04-06 02:54:53',NULL,NULL),(4,'Java for beginer',2,'A000P4','2022-04-06 02:54:53',NULL,NULL),(5,'javascript for beginer',4,'A000P5','2022-04-06 02:54:53',NULL,NULL);
/*!40000 ALTER TABLE `education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experience`
--

DROP TABLE IF EXISTS `experience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `experience` (
  `id` int NOT NULL AUTO_INCREMENT,
  `experience_name` varchar(100) DEFAULT NULL,
  `certificate` int DEFAULT NULL,
  `created_by` int NOT NULL,
  `update_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_exp_certi` (`certificate`),
  KEY `fk_exp_usr` (`created_by`),
  CONSTRAINT `fk_exp_certi` FOREIGN KEY (`certificate`) REFERENCES `certificate` (`id`),
  CONSTRAINT `fk_exp_usr` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experience`
--

LOCK TABLES `experience` WRITE;
/*!40000 ALTER TABLE `experience` DISABLE KEYS */;
INSERT INTO `experience` VALUES (1,'artificial intelgence',1,1,NULL,NULL,'2022-03-22 03:23:09'),(2,'Soware tester',2,4,NULL,NULL,'2022-03-22 03:23:09'),(3,'data analys',3,2,NULL,NULL,'2022-03-22 03:23:09'),(4,'database enginer',4,3,NULL,NULL,'2022-03-22 03:23:09'),(5,'fullstack enginer',5,3,NULL,NULL,'2022-03-22 03:23:09'),(6,'frontend',6,5,NULL,NULL,'2022-03-22 03:23:09');
/*!40000 ALTER TABLE `experience` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `has_experience`
--

DROP TABLE IF EXISTS `has_experience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `has_experience` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `exp_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_exp_user` (`user_id`),
  KEY `fk_exp_exp` (`exp_id`),
  CONSTRAINT `fk_exp_exp` FOREIGN KEY (`exp_id`) REFERENCES `experience` (`id`),
  CONSTRAINT `fk_exp_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `has_experience`
--

LOCK TABLES `has_experience` WRITE;
/*!40000 ALTER TABLE `has_experience` DISABLE KEYS */;
INSERT INTO `has_experience` VALUES (1,1,1),(2,1,2),(3,2,3),(4,3,3),(5,4,4),(6,5,5);
/*!40000 ALTER TABLE `has_experience` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `job` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) NOT NULL,
  `location` varchar(1000) NOT NULL,
  `description` text NOT NULL,
  `img` varchar(100) DEFAULT NULL,
  `like_` int NOT NULL DEFAULT '0',
  `created_by` int NOT NULL,
  `applay_by` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job`
--

LOCK TABLES `job` WRITE;
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
INSERT INTO `job` VALUES (1,'fullstack developer','Idonesian','we are open recruitments on our company send cour cv into this email example@ex.com',NULL,50,2,3,'2022-03-23 00:11:19',NULL,NULL),(2,'Database enginer','Korean','need 10 fluter proframers send ur cv at here',NULL,150,3,2,'2022-03-23 00:11:19',NULL,NULL),(3,'Typescript developer','indonesian','we are need 100 developers typescript if u interested send ur cv to this email',NULL,400,4,4,'2022-03-23 00:11:19',NULL,NULL),(4,'Spring boot developer','Indonesian','hallo everyone im a CEO on pantek company i need 200 springbioot developers send ur cv to this email',NULL,200,5,5,'2022-03-23 00:11:19',NULL,NULL),(5,'Frontend developer','Rusia','hallo everyone im senior frontend dev in drod comapany im open recruitments on staf front end developer send ur cv to email at above it',NULL,300,1,5,'2022-03-23 00:11:19',NULL,NULL),(6,'Data scientis','Rusia','Hllo everyone come on send ur cv ..and join to my comaopany',NULL,1000,1,0,'2022-03-23 04:09:54',NULL,NULL);
/*!40000 ALTER TABLE `job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `message` (
  `id` int NOT NULL AUTO_INCREMENT,
  `from_user_id` int NOT NULL,
  `to_user_id` int NOT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_applay`
--

DROP TABLE IF EXISTS `user_applay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_applay` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `job_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_applay_user` (`user_id`),
  KEY `fk_applay_job` (`job_id`),
  CONSTRAINT `fk_applay_job` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`),
  CONSTRAINT `fk_applay_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_applay`
--

LOCK TABLES `user_applay` WRITE;
/*!40000 ALTER TABLE `user_applay` DISABLE KEYS */;
INSERT INTO `user_applay` VALUES (1,3,1),(2,4,3),(3,2,2),(4,5,4),(5,3,5);
/*!40000 ALTER TABLE `user_applay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_comentar`
--

DROP TABLE IF EXISTS `user_comentar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_comentar` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `coment_id` int NOT NULL,
  `post_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_coment_user` (`user_id`),
  KEY `fk_coment_coment` (`coment_id`),
  KEY `fk_coment_job` (`post_id`),
  CONSTRAINT `fk_coment_coment` FOREIGN KEY (`coment_id`) REFERENCES `comentar` (`id`),
  CONSTRAINT `fk_coment_job` FOREIGN KEY (`post_id`) REFERENCES `job` (`id`),
  CONSTRAINT `fk_coment_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_comentar`
--

LOCK TABLES `user_comentar` WRITE;
/*!40000 ALTER TABLE `user_comentar` DISABLE KEYS */;
INSERT INTO `user_comentar` VALUES (1,1,1,1),(2,1,2,1),(3,3,4,1),(4,2,5,2),(5,5,6,6);
/*!40000 ALTER TABLE `user_comentar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_post`
--

DROP TABLE IF EXISTS `user_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_post` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `post_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_post_user` (`user_id`),
  KEY `fk_post_job` (`post_id`),
  CONSTRAINT `fk_post_job` FOREIGN KEY (`post_id`) REFERENCES `job` (`id`),
  CONSTRAINT `fk_post_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_post`
--

LOCK TABLES `user_post` WRITE;
/*!40000 ALTER TABLE `user_post` DISABLE KEYS */;
INSERT INTO `user_post` VALUES (1,1,1),(2,1,3),(3,2,2),(4,3,4),(5,1,6);
/*!40000 ALTER TABLE `user_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_send`
--

DROP TABLE IF EXISTS `user_send`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_send` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `message_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_send_user` (`user_id`),
  KEY `fk_send_message` (`message_id`),
  CONSTRAINT `fk_send_message` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`),
  CONSTRAINT `fk_send_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_send`
--

LOCK TABLES `user_send` WRITE;
/*!40000 ALTER TABLE `user_send` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_send` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password_` varchar(100) NOT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Alliano','alfarez','allianoanoanymous@gmail.com','1223liano',NULL,0,'2022-03-22 00:21:06'),(2,'Fang','manusia bayang','bayang@gmail.com','11223 shadow',NULL,0,'2022-03-22 00:21:06'),(3,'saitama','dari asgar','saitama@gmail.com','saytama991',NULL,0,'2022-03-22 00:21:06'),(4,'ultrament','nexus','nexus@gmail.com','galaxy andromeda',NULL,0,'2022-03-22 00:21:06'),(5,'james','boon','jamesngebon@gmail.com','selalu ngutang',NULL,0,'2022-03-22 00:21:06'),(8,'khalid','bin walid','khalid@admin.com','cvbsdiKK8*%*^pBBKCDSl',NULL,1,'2022-04-06 02:08:04');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-05 23:22:42
