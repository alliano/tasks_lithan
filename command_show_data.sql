-- QUERY EXPERIENCE -- 
SELECT users.first_name AS name,
experience.experience_name AS exp_name,
experience.certificate AS certificate
FROM users JOIN has_experience ON (users.id = has_experience.user_id)
JOIN experience ON (has_experience.exp_id = experience.created_by);


-- QUERY COMETAR POST -- 
SELECT * FROM user_comentar ;
SELECT * FROM job;
SELECT users.first_name AS user_post,
job.name AS job_name,
job.description AS job_desc,
job.location AS job_location,
job.like_,comentar.coment,
job.created_by AS who_is_post, 
user_comentar.user_id AS who_is_coment,
users.first_name AS user_coment
FROM users JOIN user_post ON (users.id = user_post.user_id) 
JOIN job ON (user_post.post_id = job.id) 
INNER JOIN comentar ON (comentar.created_by = users.id)
INNER JOIN user_comentar ON (comentar.created_by = users.id); 

--QUERY applay job -- 
SELECT user_applay.user_id AS id_user,
users.first_name AS who_is_applay ,
job.name AS job_name ,
job.location AS location_job
FROM users INNER JOIN user_applay ON (users.id = user_applay.user_id)
JOIN job  ON (user_applay.job_id = job.id);

-- QUERY POST JOB -- 
SELECT users.first_name AS who_post,
job.name AS job_name,
job.description,
job.location ,
job.like_
FROM users JOIN user_post ON (users.id = user_post.user_id)
INNER JOIN job ON (user_post.post_id = job.created_by);

-- QUERY USER PROFILE -- 
SELECT users.first_name AS name,
users.email AS email ,
experience.experience_name AS experience,
job.name AS job_applay,
job.location AS location
FROM users JOIN has_experience ON (users.id = has_experience.user_id)
JOIN experience ON (has_experience.exp_id = experience.id )
JOIN user_applay ON (users.id = user_applay.job_id)
LEFT JOIN job ON (users.id = job.applay_by );


-- WHO Applay education -- 
SELECT users.first_name AS name,
education.eduName AS education
FROM users INNER JOIN education ON
(users.id = education.applay_by);

-- QUERY FOR SHOW LIST ADMIN -- 
SELECT * FROM users WHERE isAdmin = TRUE;